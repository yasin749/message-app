// @ts-check
import {APIWrapper} from "./api.js";
import {EventQueue} from "./eventQueue.js";

const api = new APIWrapper();
const eventQueue = new EventQueue();

api.setEventHandler(events => {
    eventQueue.addEventList(events);
});