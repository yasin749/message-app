import {addMessage, animateGift, isPossiblyAnimatingGift, isAnimatingGiftUI} from "./dom_updates.js";
import {API_EVENT_TYPE} from "./api.js";

export class EventQueue {
    constructor() {
        this.eventQueue = [];
        this.newEventDisplayLimitPerMilisecond = 500;
        this.queueHealthCheckLimitPerMilisecond = 1000;
        this.drawInterval = null;
        this.healthCheckInterval = null;
        this.start();
    }

    start() {
        this.drawInterval = setInterval(() => {
            const currentEvent = this.getCurrentEvent();
            this.drawingEventToScreen(currentEvent);
        }, this.newEventDisplayLimitPerMilisecond);

        this.healthCheckInterval = setInterval(() => {
            this.queueHealthCheck();
        }, this.queueHealthCheckLimitPerMilisecond);
    }

    stop() {
        clearInterval(this.drawInterval);
    }

    addEventList(events) {
        if (!events || events.length === 0) return;
        events.map(event => this.setNewEvent(event));
    }

    setNewEvent(event) {
        this.eventQueue.push(event);
    }

    getCurrentEvent() {
        const isAnimatingGiftOnUI = isAnimatingGiftUI();
        let currentEvent = null;

        this.eventQueue = this.eventQueue.reduce((events, event) => {
            const isAnimatedGift = event.type === API_EVENT_TYPE.ANIMATED_GIFT;

            if (!currentEvent && !(isAnimatedGift && isAnimatingGiftOnUI)) {
                currentEvent = event;
            } else {
                events.push(event);
            }

            return events;
        }, []);

        return currentEvent;
    }

    drawingEventToScreen(event) {
        if (!event) return;

        switch (event.type) {
            case API_EVENT_TYPE.MESSAGE:
            case API_EVENT_TYPE.GIFT:
                addMessage(event);
                break;
            case API_EVENT_TYPE.ANIMATED_GIFT:
                addMessage(event);
                animateGift(event);
                break;
        }
    }

    removeOldQueueEvents(olderThanMilliseconds = 1000 * 20) {
        this.eventQueue = this.eventQueue.filter(event => Date.now() - event.timestamp < olderThanMilliseconds);
    }

    sortQueueEvents() {
        const typePriority = {
            [API_EVENT_TYPE.ANIMATED_GIFT]: 0,
            [API_EVENT_TYPE.MESSAGE]: 1,
            [API_EVENT_TYPE.GIFT]: Infinity
        };

        this.eventQueue = this.eventQueue.sort(function (x, y) {
            return x.timestamp - y.timestamp || typePriority[x.type] - typePriority[y.type];
        });

        console.log('this.eventQueue', this.eventQueue);
    }

    queueHealthCheck() {
        if (this.eventQueue === 0) return;

        this.removeOldQueueEvents();
        this.sortQueueEvents();
    }
}